package com.ecom.exceptions;

public class ProductNameNotFoundException extends RuntimeException {

	public ProductNameNotFoundException(String msg) {
		super(msg);

	}

}
