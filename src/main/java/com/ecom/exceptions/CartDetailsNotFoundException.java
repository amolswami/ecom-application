package com.ecom.exceptions;

public class CartDetailsNotFoundException extends RuntimeException {

	public CartDetailsNotFoundException(String msg) {
		super(msg);
	}

}
