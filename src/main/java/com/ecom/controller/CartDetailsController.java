package com.ecom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.dto.ProductDto;
import com.ecom.dto.ResponseDto;
import com.ecom.exceptions.CartDetailsNotFoundException;
import com.ecom.exceptions.ProductNameNotFoundException;
import com.ecom.model.Cart;
import com.ecom.service.CartService;

@RestController
public class CartDetailsController {

	@Autowired
	CartService cartService;

	@GetMapping("/cart/{cartid}")
	public ResponseEntity<Cart> getCartDetails(@PathVariable("cartid") long cartid) throws CartDetailsNotFoundException {
		Cart cartDetails = cartService.getCartDetailsByCartId(cartid);
		return new ResponseEntity<>(cartDetails, HttpStatus.OK);
	}
	
	 @PostMapping("/cart") 
	 public ResponseEntity<ResponseDto> placeOrder(@RequestBody ProductDto dto) throws ProductNameNotFoundException { 
		 ResponseDto message =cartService.addToCartProduct(dto); 
		 message.setMessage("Order Added succesfully to Cart...");
		 return new ResponseEntity<>(message, HttpStatus.OK);
	  }
	 
	
	 
}
