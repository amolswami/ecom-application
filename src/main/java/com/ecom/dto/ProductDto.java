package com.ecom.dto;

import java.util.Date;
import java.util.List;

import com.ecom.model.Product;
import com.ecom.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

public class ProductDto {

	private Date date;
	//private int quantity;
	private double price;
	
	//@JsonIgnore
	private Long uid;
	//@JsonIgnore
	private List<ProductItemDto> productItemDto;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}


	/*
	 * public List<Product> getPid() { return pid; }
	 * 
	 * @JsonManagedReference public void setPid(List<Product> pid) { this.pid = pid;
	 * }
	 */
	
	

	public Date getDate() {
		return date;
	}

	public List<ProductItemDto> getProductItemDto() {
		return productItemDto;
	}

	public void setProductItemDto(List<ProductItemDto> productItemDto) {
		this.productItemDto = productItemDto;
	}

	public void setDate(Date date) {
		this.date = date;
	}



}
