package com.ecom.productControllerTest;

import com.ecom.controller.ProductController;
import com.ecom.dto.ResponseDto;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.model.User;
import com.ecom.model.UserOrder;
import com.ecom.service.ProductService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductControllerTest {

	@InjectMocks
	ProductController productController;

	@Mock
	ProductService productService;

	@Test
	public void testsearchProductForPositive() {
		List<Product> products = new ArrayList<Product>();
		Product p = new Product();
		Product product = new Product();
		product.setPid(1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);
		Mockito.when(productService.viewProductByName("iphone")).thenReturn(products);
		ResponseEntity<List<Product>> product1 = productController.viewProductName("iphone");
		Assert.assertNotNull(product1);
		Assert.assertEquals(product1.getStatusCode(), HttpStatus.OK);

	}

	@Test
	public void testsearchProductForNegative() {
		List<Product> products = new ArrayList<Product>();
		Product p = new Product();
		Product product = new Product();
		product.setPid(-1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);
		Mockito.when(productService.viewProductByName("iphone")).thenReturn(products);
		ResponseEntity<List<Product>> product1 = productController.viewProductName("iphone");
		Assert.assertNotNull(product1);
		Assert.assertEquals(product1.getStatusCode(), HttpStatus.OK);

	}

	@Test
	public void testPurchaseOrderForPositive() {
		List<Product> products = new ArrayList<Product>();
		Product p = new Product();
		Cart cart = new Cart();
		User user = new User();
		UserOrder order = new UserOrder();

		user.setUid(101l);
		user.setUname("Prajakta");
		user.setPassword("prajakta");
		user.setAddress("address");
		user.setUcontactNumber("678908876");
		user.setUsername("username");

		Product product = new Product();
		product.setPid(1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);

		cart.setCartid(1l);
		cart.setDate(new Date());
		cart.setPrice(600.0);
		cart.setProduct(product);
		cart.setQuantity(2);
		cart.setUser(user);

		order.setDate(new Date());
		order.setId(3l);
		order.setTotalprice(200.89);

		ResponseDto dto = new ResponseDto();
		Mockito.when(productService.purchaseOrder(2l)).thenReturn(dto);
		ResponseEntity<ResponseDto> product1 = productController.purchaseOrder(1l);
		Assert.assertNotNull(product1);
		Assert.assertEquals(dto.getMessage(), HttpStatus.OK);

	}

	@Test
	public void testPurchaseOrderForNegative() {

		Product p = new Product();
		Cart cart = new Cart();
		User user = new User();

		user.setUid(-101l);
		user.setUname("Prajakta");
		user.setPassword("prajakta");
		user.setAddress("address");
		user.setUcontactNumber("678908876");
		user.setUsername("username");

		Product product = new Product();
		product.setPid(1l);
		product.setPname("iphone");
		product.setPdesc("mobile");

		cart.setCartid(1l);
		cart.setDate(new Date());
		cart.setPrice(600.0);
		cart.setProduct(product);
		cart.setQuantity(2);
		cart.setUser(user);
		ResponseDto dto = new ResponseDto();

		Mockito.when(productService.purchaseOrder(2l)).thenReturn(dto);
		ResponseEntity<ResponseDto> res = productController.purchaseOrder(2l);
		Assert.assertNotNull(cart);
		Assert.assertEquals(dto.getMessage(), HttpStatus.OK);

	}

}
