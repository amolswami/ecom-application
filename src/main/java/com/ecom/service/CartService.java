package com.ecom.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecom.dto.ProductDto;
import com.ecom.dto.ProductItemDto;
import com.ecom.dto.ResponseDto;
import com.ecom.exceptions.CartDetailsNotFoundException;
import com.ecom.exceptions.ProductNameNotFoundException;
import com.ecom.exceptions.UserNotFoundException;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.model.User;
import com.ecom.repository.CartRepository;
import com.ecom.repository.ProductRepository;
import com.ecom.repository.UserRepository;

@Service
public class CartService {

	@Autowired
	CartRepository cartRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProductRepository productRepository;

	public Cart getCartDetailsByCartId(long cartid) {

		Optional<Cart> cartDetails = cartRepository.findById(cartid);

		Cart cart = null;
		if (cartDetails.isPresent()) {
			cart = cartDetails.get();
		} else {
			throw new CartDetailsNotFoundException("This CartDetails is not available");
		}

		return cart;
	}

	public ResponseDto addToCartProduct(ProductDto dto) {

		System.out.println("Inside ADD Method");
		double totalPrice = 0;
		Cart cart = new Cart();

		User user = userRepository.findById(dto.getUid())
				.orElseThrow(() -> new UserNotFoundException("User with given id not found"));
		cart.setUser(user);
		System.out.println("Aftrer User");

		List<ProductItemDto> itemIds = dto.getProductItemDto();
		for (int i = 0; i < itemIds.size(); i++) {

			System.out.println("Inside for");
			Product item = productRepository.findById(itemIds.get(i).getPid())
					.orElseThrow(() -> new ProductNameNotFoundException("Product with given id not found"));

			int quant = itemIds.get(i).getQuantity();
			totalPrice = totalPrice + (quant * item.getPrice());
			System.out.println(totalPrice + "Total");
			cart.setDate(dto.getDate());
			cart.setPrice(totalPrice);
			cart.setProduct(item);
			cart.setQuantity(quant);

		}
		cartRepository.save(cart);
		return new ResponseDto();

	}

}
