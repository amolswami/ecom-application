package com.ecom.cartServiceTest;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ecom.exceptions.CartDetailsNotFoundException;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.repository.CartRepository;
import com.ecom.service.CartService;

@RunWith(MockitoJUnitRunner.Silent.class)
	public class CartDetailsServiceTest {
	    
	    @InjectMocks
	    CartService cartService;
	    
	    @Mock
	   CartRepository cartRepository;
	    

	 

	    @Test
	    public void testGetPoliciesByIdForPositive() {
	        Cart cart = new Cart(); 
	        cart.setCartid(1l);
	        cart.setPrice(100);
	        Product p=new Product();
	        p.setPid(1);
	        p.setPdesc("mobile");
	        p.setPrice(1000);
	        cart.setProduct(p);
	        
	        Mockito.when(cartRepository.findById(1l)).thenReturn(Optional.of(cart));
	        Assert.assertNotNull(cart);
	    }

	 

	    @Test
	    public void testFindByIdForPositive() {
	        Cart cart = new Cart(); 
	        cart.setCartid(1l);
	        cart.setPrice(100);
	        Product p=new Product();
	        p.setPid(1);
	        p.setPdesc("mobile");
	        p.setPrice(1000);
	        cart.setProduct(p);
	        Mockito.when(cartRepository.findById(1l)).thenReturn(Optional.of(cart));
	        Cart resCart = cartService.getCartDetailsByCartId(1l);
	        Assert.assertNotNull(resCart);
	        Assert.assertEquals(cart.getCartid(), resCart.getCartid());
	    }

	 

	    @Test(expected = CartDetailsNotFoundException.class)
	    public void testFindByPolicyIdForExce() {
	        Cart cart = new Cart(); 
	        cart.setCartid(2l);
	        cart.setPrice(100);
	        Mockito.when(cartRepository.findById(2l)).thenReturn(Optional.of(cart));
	        Cart resCarts = cartService.getCartDetailsByCartId(3l);
	    }

	 

	    
	}


