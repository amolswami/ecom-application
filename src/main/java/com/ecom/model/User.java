package com.ecom.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long uid;
	private String uname;
	private String ucontactNumber;
	private String address;
	private String username;
	private String password;
	
	@OneToOne(mappedBy = "user")
	private Cart cart;

	public User() {
		System.out.println("user controller----");
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUcontactNumber() {
		return ucontactNumber;
	}

	public void setUcontactNumber(String ucontactNumber) {
		this.ucontactNumber = ucontactNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}
