package com.ecom.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.dto.ProductDto;
import com.ecom.dto.ResponseDto;
import com.ecom.exceptions.CartDetailsNotFoundException;
import com.ecom.exceptions.ProductNameNotFoundException;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.model.UserOrder;
import com.ecom.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;
	

	@GetMapping("/products/{pname}")
	public ResponseEntity<List<Product>> viewProductName(@PathVariable("pname") String pname)
			throws ProductNameNotFoundException {
		List<Product> products = productService.viewProductByName(pname);
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}
	 
	 @PostMapping("/order/{cartid}")
	    public ResponseEntity<ResponseDto> purchaseOrder(@PathVariable("cartid")long cartid) {
	        ResponseDto message = productService.purchaseOrder(cartid);
	        message.setMessage("Order placed succesfully...");
	        return new ResponseEntity<>(message, HttpStatus.OK);
	    }
	 
}
