package com.ecom.cartControllerTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ecom.controller.CartDetailsController;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.service.CartService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CartDetailsControllerTest
{

	​@InjectMocks
	CartDetailsController cartDetailsController;

	@Mock
	CartService CartService;

	@Test
	    public void testForGetCartById() {​​​​​​​
	        Cart cart = new Cart(); 
	        cart.setCartid(1l);
	        cart.setPrice(100);
	        Product p=new Product();
	        p.setPid(1);
	        p.setPdesc("mobile");
	        p.setPrice(1000);
	        cart.setProduct(p);


	        Mockito.when(CartService.getCartDetailsByCartId(1l)).thenReturn(cart);


	        ResponseEntity<Cart> poly = cartDetailsController.getCartDetails(1l);
	        Assert.assertNotNull(poly);
	        Assert.assertEquals(HttpStatus.OK, poly.getStatusCode());


	    }​ ​ ​ ​ ​ ​ ​
}​ ​ ​ ​ ​ ​ ​

}
