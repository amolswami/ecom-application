package com.ecom.productServiceTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ecom.dto.ResponseDto;
import com.ecom.exceptions.ProductNameNotFoundException;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.model.User;
import com.ecom.model.UserOrder;
import com.ecom.repository.CartRepository;
import com.ecom.repository.ProductRepository;
import com.ecom.repository.UserOrderRepository;
import com.ecom.service.ProductService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceTest {

	@InjectMocks
	ProductService productService;

	@Mock
	ProductRepository productRepository;

	@Mock
	CartRepository cartRepo;

	UserOrderRepository userOrderRepository;
	@Test(expected = ProductNameNotFoundException.class)
	public void testProductForPositive() {
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();

		product.setPid(1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);
		Mockito.when(productRepository.findByPnameLike("iphone")).thenReturn(products);
		List<Product> productss = productService.viewProductByName("iphone");
		Assert.assertNotNull(productss);
		Assert.assertEquals(1, productss.size());

	}

	@Test(expected = ProductNameNotFoundException.class)
	public void testProductForNegative() {
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();

		product.setPid(-1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);
		Mockito.when(productRepository.findByPnameLike("iphone")).thenReturn(products);
		List<Product> productss = productService.viewProductByName("iphone");
		Assert.assertNotNull(productss);
		Assert.assertEquals(1, productss.size());

	}

	@Test(expected = ProductNameNotFoundException.class)
	public void testProductForException() {
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();

		product.setPid(1l);
		product.setPname("iphone");
		product.setPdesc("mobile");
		products.add(product);
		Mockito.when(productRepository.findByPnameLike("iphone")).thenReturn(products);
		List<Product> productss = productService.viewProductByName("iphone");
		Assert.assertNotNull(productss);
		Assert.assertEquals(2, productss.size());

	}

	@Test(expected = ProductNameNotFoundException.class)
	public void testPlacrOrderForPositive() {
		
		UserOrder order = new UserOrder();
		
		Product p = new Product();
		order.setId(1l);
		order.setDate(new Date());
		order.setTotalprice(778.00);
		Mockito.when(userOrderRepository.save(Mockito.any(UserOrder.class))).thenReturn(order);
		ResponseDto productss = productService.purchaseOrder(1l);
		Assert.assertNotNull(productss);
		Assert.assertEquals(1, productss.getMessage());

	}
	
	@Test(expected = ProductNameNotFoundException.class)
	public void testPlacrOrderForNegative() {
		
		UserOrder order = new UserOrder();
		
		Product p = new Product();
		order.setId(-1l);
		order.setDate(new Date());
		order.setTotalprice(-778.00);
		Mockito.when(userOrderRepository.save(Mockito.any(UserOrder.class))).thenReturn(order);
		ResponseDto productss = productService.purchaseOrder(1l);
		Assert.assertNotNull(productss);
		Assert.assertEquals(1, productss.getMessage());

	}

}
