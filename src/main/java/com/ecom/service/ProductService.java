package com.ecom.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecom.dto.ResponseDto;
import com.ecom.exceptions.CartDetailsNotFoundException;
import com.ecom.exceptions.ProductNameNotFoundException;
import com.ecom.exceptions.UserNotFoundException;
import com.ecom.model.Cart;
import com.ecom.model.Product;
import com.ecom.model.UserOrder;
import com.ecom.repository.CartRepository;
import com.ecom.repository.ProductRepository;
import com.ecom.repository.UserOrderRepository;
import com.ecom.repository.UserRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private UserOrderRepository orderRepository;

	public List<Product> viewProductByName(String pname) throws ProductNameNotFoundException {

		List<Product> products = productRepository.findByPnameLike("%" + pname + "%");

		if (products.isEmpty()) {

			throw new ProductNameNotFoundException("Product is not available");

		} else {

			return products;

		}

	}

	public ResponseDto purchaseOrder(long cartid) {
		UserOrder order = new UserOrder();
		Cart cart = cartRepository.findById(cartid)
				.orElseThrow(() -> new CartDetailsNotFoundException("No Cart Details Available"));
		order.setId(cartid);
		order.setDate(new Date());
		order.setTotalprice(cart.getPrice());

		 orderRepository.save(order);
		 return new ResponseDto();

	}

}
